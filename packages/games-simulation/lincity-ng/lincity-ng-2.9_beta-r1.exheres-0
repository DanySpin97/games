# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ tag=${PN}-${PV/_beta/-beta} ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] \
    jam \
    gtk-icon-cache \
    freedesktop-desktop

SUMMARY="A city simulation game"
DESCRIPTION="
It is a polished and improved version of the classic LinCity game. In the
game, you are required to build and maintain a city. You can win the game
either by building a sustainable economy or by evacuating all citizens with
spaceships.
"

LICENCES="
    GPL-2
    ( || ( GPL-2 CCPL-Attribution-ShareAlike-2.0 ) ) [[ description = [ Game DATA, see COPYING-data ] ]]
    ( Bitstream-Vera Arev ) [[ description = [ included Bitstream, DejaVu fonts ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext
    build+run:
        dev-libs/libxml2:2.0[>=2.6.11]
        dev-games/physfs[>=1.0.0]
        media-libs/SDL:0[>=1.2.14][X]
        media-libs/SDL_gfx[>=2.0.13]
        media-libs/SDL_image:1[>=1.2.1]
        media-libs/SDL_mixer:0[>=1.2.2]
        media-libs/SDL_ttf:0[>=2.0.8]
        x11-dri/mesa [[ note = [ provides libGL ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.0-build.patch
    "${FILES}"/064ba3ac775137b5a348d0df19186073b891be39.patch
    "${FILES}"/1a9e9cde8ea5d2cd4fc6203e7e619f23064f8960.patch
    "${FILES}"/240053f1d46062b8610eb312c6b02e6f88e28794.patch
    "${FILES}"/fe87dfe4dde4e495b38f3e079df2e67327041dec.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-optimize )

src_prepare() {
    default

    edo ./autogen.sh

    edo sed -re '/(C|CXX)FLAGS/s:-O3 -g ::' -i Jamrules

    # install fails otherwise
    edo touch CREDITS
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

