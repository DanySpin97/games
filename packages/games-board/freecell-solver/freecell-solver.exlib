# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ] python [ blacklist=2 multibuild=false ]

export_exlib_phases src_prepare src_install

SUMMARY="A program that automatically solves layouts of Freecell and similar variants"
DESCRIPTION="
It ca solve Freecell and similar variants of Card Solitaire such as Eight Off,
Forecell, and Seahaven Towers, as well as Simple Simon boards. Included with
the archive are programs that can automatically provide the starting layouts
of several popular Solitaire Implementations.
Freecell Solver can also be built as a library for use within your own
Solitaire implementations."

HOMEPAGE="http://fc-solve.shlomifish.org/"
DOWNLOADS="${HOMEPAGE}downloads/fc-solve/${PNV}.tar.xz"

LICENCES="
    MIT
    BSD-3 [[ note = [ kaz_tree.{c,h} ] ]]
"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
        dev-util/gperf
    build+run:
        dev-perl/Games-Solitaire-Verify
        dev-perl/Path-Tiny
        dev-perl/Template-Toolkit
    build+test:
        dev-python/pysol-cards[python_abis:*(-)?]
        dev-python/random2[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
    test:
        dev-libs/gmp:*
        dev-libs/libtap[>=1.14.0]
        dev-perl/Inline
        dev-perl/Perl-Tidy
        dev-perl/Task-FreecellSolver-Testing
        dev-perl/Test-Data-Split
        dev-perl/Test-Differences
        dev-perl/Test-RunValgrind
        dev-perl/Test-TrailingSpace
        dev-python/pycotap[python_abis:*(-)?]
"

BUGS_TO="heirecka@exherbo.org"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}docs/"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_STATIC_LIBRARY:BOOL=FALSE
    # Man pages are already included in the tarball, would need asciidoctor
    -DFCS_BUILD_DOCS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS+=( '-DFCS_WITH_TEST_SUITE:BOOL=TRUE -DFCS_WITH_TEST_SUITE:BOOL=FALSE' )

freecell-solver_src_prepare() {
    cmake_src_prepare

    edo sed -e "s|\(share/man/man\)|/usr/\1|" \
        -e "/SET (DATADIR/ s|\${CMAKE_INSTALL_PREFIX}|/usr|" \
        -i cmake/Shlomif_Common.cmake

    edo sed -e "s|\${RELATIVE_PKGDATADIR}|/usr/share/freecell-solver|" \
        -i Presets/CMakeLists.txt \
        -i Presets/presets/CMakeLists.txt

    # Remove another failing test (5.0.0)
    edo rm t/t/py-flake8.t
    # Wants to use ag (dev-util/the_silver_search)
    edo rm t/t/spelling.t
}

freecell-solver_src_install() {
    cmake_src_install

    # We don't need a win32 readme, and we don't need it in a wrong dir
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/share
}

