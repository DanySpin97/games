# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require lua [ whitelist=5.3 multibuild=false ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="FreedroidPRG is a mature science fiction role playing game set in the future"
DESCRIPTION="
FreedroidRPG is an isometric RPG game inspired by Diablo and Fallout. There is
a war, and we are losing it. No one really knows why the bots started killing
us. Most don't even want to know. The human race is reduced to little more than
a few isolated groups of people trying to survive. Somewhere in a place known
as Temple Wood, a hero is about to wake up from a long sleep...
"
HOMEPAGE="http://www.freedroid.org"
DOWNLOADS="https://ftp.osuosl.org/pub/freedroid/${PN%rpg}RPG-$(ever range 1-2)/${PN%rpg}RPG-${PV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/SDL:0[>=1.2.3]
        media-libs/SDL_gfx[>=0.0.21]
        media-libs/SDL_image:1
        media-libs/SDL_mixer:0[>=1.2.1]
        x11-dri/glu
        x11-dri/mesa
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    run:
        x11-apps/xdg-utils
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-opengl
    --enable-sound
    --disable-backtrace
    --disable-debug
    --disable-dev-tools
    --disable-fastmath
    --disable-native-arch
    --disable-rtprof
    --disable-sanitize-address
    --with-open-cmd=xdg-open
    --without-embedded-lua
    --without-extra-warnings
)

src_configure() {
    export LUA_CFLAGS=$(${PKG_CONFIG} --cflags lua-$(lua_get_abi))
    export LUA_LIBS=$(${PKG_CONFIG} --libs lua-$(lua_get_abi))
    export LUAC=/usr/$(exhost --target)/bin/luac$(lua_get_abi)

    default
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

